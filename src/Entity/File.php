<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File as FileInfo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 * @Vich\Uploadable
 */
class File
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $fileName;

    /**
     * @Vich\UploadableField(mapping="files", fileNameProperty="fileName")
     * @var FileInfo
     */
    private $fileFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $dateUploaded;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setFileFile(FileInfo $fileFile = null)
    {
        $this->fileFile = $fileFile;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($fileFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateUploaded = new \DateTime('now');
        }
    }

    public function getFileFile()
    {
        return $this->fileFile;
    }

    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function getDateUploaded()
    {
        return $this->dateUploaded;
    }
}
