<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController;
use Parsedown;

class DefaultController extends AdminController
{
    /**
     * @Route("/application/", name="application")
     */
    public function indexAction(Request $request)
    {
        $response = parent::indexAction($request);

        $this->checkPermissions($request);
        return $response;
    }

    /**
     * @Route("/", name="about")
     */
    public function about()
    {
        $Parsedown = new Parsedown();
        return $this->render('default/about.html.twig', array(
            'readme' => $Parsedown->text(file_get_contents('../README.md')),
        ));
    }

    /**
     * @Route("/todo", name="todo")
     */
    public function todo()
    {
        $Parsedown = new Parsedown();
        return $this->render('default/todo.html.twig', array(
            'readme' => $Parsedown->text(file_get_contents('../TODO.md')),
        ));
    }

    private function checkPermissions($request)
    {
        $easyAdmin = $request->attributes->get('easyadmin');
        if(empty($easyAdmin['entity']['require_permission'])) {
            return;
        }
        if (isset($easyAdmin['entity']['require_permission'])) {
            $requiredPermission = $easyAdmin['entity']['require_permission'];
        } else {
            $view = $easyAdmin['view'];
            $entity = $easyAdmin['entity']['name'];
            $requiredPermission = 'ROLE_'.strtoupper($view).'_'.strtoupper($entity);
            # Or any other default strategy
        }
        $this->denyAccessUnlessGranted(
            $requiredPermission, null, $requiredPermission.' permission required'
        );
    }

    /**
     * Generates the backend homepage and redirects to it.
     */
    protected function redirectToBackendHomepage()
    {
        $homepageConfig = $this->config['homepage'];
        foreach($this->config['entities'] as $entity) {
            if(empty($entity['require_permission']) || $this->isGranted($entity['require_permission'])) {
                $homepageConfig['params']['entity'] = $entity['name'];
                break;
            }
        }
        $url = isset($homepageConfig['url'])
            ? $homepageConfig['url']
            : $this->get('router')->generate($homepageConfig['route'], $homepageConfig['params']);

        return $this->redirect($url);
    }
}
