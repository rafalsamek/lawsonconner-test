<?php
namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('user1');
        $user->setFullName('John Smith');
        $user->setEmail('user1@example.com');
        $user->setPassword('user1');
        $manager->persist($user);
        $manager->flush();

        // other fixtures can get this object using the 'user1' name
        $this->addReference('user1', $user);

        $user = new User();
        $user->setUsername('user2');
        $user->setFullName('Alice Smith');
        $user->setEmail('user2@example.com');
        $user->setPassword('user2');
        $manager->persist($user);
        $manager->flush();

        // other fixtures can get this object using the 'user2' name
        $this->addReference('user2', $user);
    }
}
