# lawsonconner-test

##Lawson Conner Developer Test

Create PHP/MySQL application aimed to do a basic document share between the system Administrator and the users of the system.

###The Admin Panel should consist of the following three screens:

1. **User management** with minimum Add & Edit functions. User data should consist of: Username, Full Name, Email, Password.

2. **File list** should display the list of all previously uploaded files. The list should have four columns: File name, Date uploaded, Viewed (list of users who viewed the file), Pending (list of users who haven’t yet viewed the file). 

3. **File upload** should allow the admin to upload files and assign them to one or more regular users. When a new file is uploaded, it is displayed accordingly in the File list of the admin and the user.

###User panel should consist of just one screen:
1. **File list**, where each user can view all the files were assigned to him. The list should have three columns: File name, Date assigned, Status (viewed/not viewed) and  a “View” button for each file, allowing the preview/download of the file in a popup window.

###Task

The backend of the application should have an MVC design pattern, and the code should be object oriented PHP. It is entirely up to you to decide if you will use or not a PHP framework, popular or custom. All functional applications are welcomed. 

Writing sample unit tests for the most likely errors and adding features you might think are useful are a plus.

Please return the test result with test instructions and sample data.


###Installation steps

1. Clone the project and configure it using following commands:
    
        git clone https://bitbucket.org/rafalsamek/lawsonconner-test lawsonconner-test
        composer install
    
2. Copy .env.dist to .env in root directory and modify database part
        
        DATABASE_URL=mysql://root:123@127.0.0.1:3306/lawsonconner-test

3. Create database manually or using doctrine
        
        php bin/console doctrine:database:create

4. Create schema

        php bin/console doctrine:schema:create
        
5. Or run db migrations

        php bin/console doctrine:migrations:migrate
        
6. Load data fixtures

        php bin/console doctrine:fixtures:load
    
7. Start the Symfony built-in server or create a virtual host:

        php bin/console server:start
    
    or to configure virtual host in apache, run the following command:
    
        sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/example.dev.conf
    
    Here example.dev is a sample virtual host name. Change it accordingly. Now edit it:
    
        sudo nano /etc/apache2/sites-available/example.dev.conf
    
    Here's an example contents for this file:

        <VirtualHost *:80>
            ServerAdmin admin@localhost
            ServerName example.dev
            ServerAlias www.example.dev
            DocumentRoot /var/www/project/public
            ErrorLog ${APACHE_LOG_DIR}/error.log
            CustomLog ${APACHE_LOG_DIR}/access.log combined
        </VirtualHost>

    Then you will need to run the following commands to enable virtual host:

        sudo a2ensite example.dev.conf
        sudo service apache2 restart
    
###Running Behat tests

From the project root directory:

    php bin/behat
    
###Running Unit tests

From the project root directory:

    phpunit

###Viewing the site

[http://localhost:8000](http://localhost:8000)

or the example.dev if you created virtual host as above

[http://example.dev](http://example.dev)

Also you can see the whole application site here:

[http://lawsonconner-test.rafalsamek.pl](http://lawsonconner-test.rafalsamek.pl)

####Login to the Application

1. In the left menu click on "Application"

2. In the login page you can login with following credentials:

    Admin: (stored in memory - /config/packages/security.yaml)
    
        admin/admin
    
    User: (from fixtures, stored in database)
    
        user1/user1
        user2/user2
        
    As an Admin in User Management you can add/edit/delete users and use them for authorization
    
        username/password   