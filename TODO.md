###TODO

1. Admin: File List: create - major - partly done - functional
* add multiple options: Users Assigned - major
2. Admin: File List: edit - major - partly done -  functional
* remove the delete file option from edit form - minor
* add multiple options: Users Assigned - major
3. Admin: File List: view - minor
* put in a popup window - minor
4. Admin: File List: list - major - partly done - functional
* change actions icons: view, edit, delete - minor
* add column Viewed: comma seperated usernames - major
* add column Pending: comma seperated usernames - major
5. UserFile entity - major - partly done
* add user_id column - major
* add file_id column - major
6. User: File List: list - major - partly done
* add File name column - major
* add Date assigned column - major
7. Add Status column (Viewed/Not viewed) - major
8. User: File List: view - major
* put in a popup window - major
9. Data Fixtures
* users - major - done
* files - major
* user_files - major